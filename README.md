# ![The Swiss Pathogens Portal](static/images/portal.png)

## The Swiss Pathogens Portal

This is the source code of the Swiss Pathogens Portal [https://pathogensportal.ch](https://pathogensportal.ch). 

This website is the Swiss node for the European Pathogens Portal project.
The main European site can be seen at [https://www.pathogensportal.org](https://www.pathogensportal.org/)

## Introduction

This website is developed and maintained by the [SIB Swiss Institute of Bioinformatics](https://www.sib.swiss/)

The site is built using the [Hugo](https://gohugo.io/) static website generator. It uses the [HUGO Pathogens Portal Theme](https://github.com/sib-swiss/hugo-pathogens-portal).

## Cite this portal

The RRID for the Swiss Pathogens Portal is **SCR_025457**.

## Credits

The Swiss Pathogens Portal is developed and maintained by the [SIB Swiss Institute of Bioinformatics](https://www.sib.swiss/).