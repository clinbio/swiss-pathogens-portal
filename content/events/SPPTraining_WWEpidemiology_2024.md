---
title: "PEAK Course - Epidemiology based on wastewater - health data for Switzerland"
start_date: 2024-09-27T08:00:00Z
end_date: 2024-09-27T17:00:00Z
location: Dübendorf, Switzerland
description: The analysis of pathogens and chemical substances in wastewater can provide information on the spread of infectious diseases and the consumption of medications within the population. During the COVID-19 pandemic, wastewater-based epidemiology quickly gained popularity as it allows for monitoring the evolution of the pandemic independently of clinical case reporting. This course presents the various activities currently being carried out in this field in Switzerland, describes the different possible applications of the method in public health, and offers an overview of future prospects. The speakers are experts from academia, industry, and the health sector at both the cantonal and federal levels.
organiser: Eawag PEAK
external_url: https://www.eawag.ch/fr/portail/agenda/detail/?tx_sfpevents_sfpevents%5Bcontroller%5D=Events&tx_sfpevents_sfpevents%5Bevent%5D=2435&cHash=dc8e52765ca70b0d8cdcf489e0846838
categories: 
  - Wastewater
---