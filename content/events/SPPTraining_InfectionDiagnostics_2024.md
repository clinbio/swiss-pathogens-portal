---
title: "The Future of Infection Diagnostics: A Comprehensive Guide to Next-Generation Sequencing."
start_date: 2024-09-02T08:00:00Z
end_date: 2024-09-04T17:00:00Z
location: Lørenskog, Norway
description: Next-generation sequencing (NGS) has become widely known through its role in addressing the COVID-19 pandemic. However, its benefits extend far beyond tracking viruses. This three-day workshop will explore what, how and when to use NGS through real-world case studies. Attendees will learn about different sequencing platforms and types of tests and see examples of when each is best applied in clinical settings. Two practical bioinformatics sessions will give hands-on experience analysing NGS data. By the end, participants will have a clear understanding of how to harness this powerful technology in their work. This "How to..." workshop has something for everyone from professionals new to the field to experts looking to expand their toolkit.
organiser: Akershus University Hospital Norway, University Medical Center Groningen Netherlands and study groups of the European Society for Clinical Microbiology and Infectious Diseases (ESGMD, ESGEM, TAE).
external_url: https://www.escmid.org/event-detail/the-future-of-infection-diagnostics-a-comprehensive-guide-to-next-generation-sequencing/
categories: 
  - NGS
  - Diagnostic
---