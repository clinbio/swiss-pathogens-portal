---
title: Research
menu: 
  main:
    name: Research
    identifier: research
    weight: 4
---

This is a manually curated overview of infectious diseases and antimicrobial research projects, which are funded by major funding agencies in Switzerland. New projects are added on an ongoing basis. If you would like your project to be listed here, please get in touch with us.

------------------------------------------------------------
| Title | Short description                                                                               | Main PIs                                                                                                        | Project duration and funding agency | Website                                                                                                            |
|-------|-------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|-------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| IICU  | Personalized, data-driven prediction and assessment of Infection related outcomes in Swiss ICUs | Prof. Adrian Egli (UZH), Prof. Catherine Jutzeler (ETHZ), Prof. Karsten Borgwardt (ETHZ, until 1 February 2023) | 2022 - 2025, funded by SPHN         | [https://sphn.ch/network/projects/project-page_nds_iicu/](https://sphn.ch/network/projects/project-page_nds_iicu/) |
