---
title: Contribute
menu:
  navbar_top:
    name: Contribute
    weight: 3
---

The Swiss Pathogens Portal was built to serve the research community with data and resources generated and maintained primarily by Swiss groups or institutions.

Please do not hesitate to reach out to the Portal team if you wish to contribute or suggest modifications (see [Contact form](/contact-suggestions/)).

## Send GitLab pull requests

We also welcome new members in the editorial team. If you feel like contributing substantially or on a regular basis, please contact us and we will add you to the members of the GitLab project.

All information which is displayed on the Portal is stored on GitLab as Markdown files, making it human-readable and easy to edit. If you know your way around the web interface of GitLab, you can easily edit or add information yourself and send us a pull request.

The information for each section of the Portal is stored in a particular folder and needs to be formatted in a certain way. To make it easier for you to contribute, there is detailed instructions for each of the sections.