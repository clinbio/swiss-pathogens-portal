---
title: Available Datasets
menu:
  share_access_data:
    name: Available Datasets
    weight: 2
  footer_data:
    name: Datasets
    weight: 2
layout: datasets
---

## Access-controlled datasets

This is a manually curated list of available datasets with controlled-access. New datasets are added on an ongoing basis. If you would like your project to be listed here, please get in touch with us.

------------------------------------------------------------
| Name                                 | Access data catalog            | Data access documentation                                      | Contact                                  |
|--------------------------------------|--------------------------------|----------------------------------------------------------------|------------------------------------------|
| Swiss Pathogen Surveillance Platform | https://public.spsp.sib.swiss/ | https://public.spsp.sib.swiss/docs/data-access-and-re-use.html | aitana.neves@sib.swiss; aegli@imm.uzh.ch |
------------------------------------------------------------