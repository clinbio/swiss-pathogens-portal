---
title: Share Data
menu:
  share_access_data:
    name: Share Data
    weight: 1
---

## FAIR Data

The Swiss National Science Foundation (SNSF) encourages researchers to share their data in alignment with the FAIR Data Principles, which promote the Findability, Accessibility, Interoperability, and Reusability of data. Researchers are expected to make their data available in publicly accessible digital repositories. For more details, you can visit the SNSF’s research policies page here (https://www.snf.ch/en/FAiWVH4WvpKvohw9/topic/research-policies).
For researchers dealing with biomolecular data, the ELIXIR Deposition Databases for Biomolecular Data serve as a valuable resource. This platform helps you identify recommended databases for depositing your data, ensuring compliance with best practices in data sharing. You can explore these resources here (https://elixir-europe.org/platforms/data/elixir-deposition-databases).

## Guidelines for Pathogen Genomic Data Submission

When working with pathogen genomic data, it is essential to follow specific guidelines based on the nature of your metadata:
* Non-Sensitive Metadata: If your metadata does not contain sensitive personal information, you can directly submit your data to the European Nucleotide Archive (ENA). To do so, you will need to register on their submission portal, which can be accessed here (https://www.ebi.ac.uk/ena/browser/submit).
* Sensitive Personal Data: If your metadata includes sensitive personal data, it is recommended to submit your data to the Swiss Pathogen Surveillance Platform (SPSP). At SPSP, your data will be carefully curated and annotated before being shared with the ENA under a robust ethical and legal framework. Detailed submission instructions for new registered groups can be found here (https://public.spsp.sib.swiss/docs/instructions-for-new-registered-groups.html).
SPSP also accepts submissions of data with non-sensitive metadata. To begin the submission process, registration on the SPSP website is required, which can be done here (https://spsp.ch/register-group/).

By following these guidelines, you ensure that your pathogen genomic data is shared responsibly and in accordance with ethical standards, facilitating further research and collaboration within the scientific community.
