---
title: "SIB launches the Swiss Pathogens Portal: A new hub for pathogen research and news"
date: 2024-08-28T13:03:06+02:00
draft: false
author: "Aitana Neves"
image: news/launch.png
tags:
  - portal
---
# SIB launches the Swiss Pathogens Portal: A new hub for pathogen research and news

SIB Swiss Institute of Bioinformatics is proud to announce the launch of the Swiss Pathogens Portal, a community-focused
portal designed to facilitate pathogen research and collaboration across the nation. Linked to
the [Pathogens Portal](https://www.pathogensportal.org/) hosted at EMBL-EBI and inspired from
the [Swedish Pathogens Portal](https://www.pathogens.se/), this new resource serves as a centralized hub for
researchers, healthcare professionals, and policymakers to
facilitate access to data, tools and resources related to infectious diseases research and surveillance. In addition to
the launch, the SIB has developed a versatile [HUGO Pathogens Portal theme](https://github.com/sib-swiss/hugo-pathogens-portal), enabling other countries and
institutions to rapidly create similar portals. This initiative underscores SIB's commitment to FAIR data for advancing
research and fostering collaboration in the fight against infectious diseases. Funded by
the [BY-COVID project](https://by-covid.org).