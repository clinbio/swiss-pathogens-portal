---
title: "Infectious diseases dashboard"
description: "Information on cases of infection and illness in Switzerland and the Principality of Liechtenstein caused by various pathogens, provided by the Federal Office of Public Health."
image: dashboards/infectious-diseases-dashboard_logo.png
redirect_url: https://www.idd.bag.admin.ch/
highlight: true
tags:
    - Surveillance
    - Infectious diseases
---