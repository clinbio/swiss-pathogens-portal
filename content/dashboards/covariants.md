---
title: "Overview of SARS-CoV-2 variants and mutations"
description: "Find out what mutations define a variant, what impact they might have (with links to papers and resources), where variants are found, and see the variants in Nextstrain builds."
image: dashboards/covariants_logo.svg
redirect_url: https://covariants.org/
tags:
    - SARS-CoV-2
    - portal
---

