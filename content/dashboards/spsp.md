---
title: "Swiss Pathogen Surveillance Platform"
description: "A secure One-health online platform that enables near real-time sharing of pathogen sequencing data and their associated clinical/epidemiological metadata."
image: dashboards/spsp_logo.png
redirect_url: https://spsp.ch/
highlight: true
tags:
    - Surveillance
    - SARS-CoV-2
    - Influenza
    - Bacteria
---

