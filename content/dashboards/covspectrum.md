---
title: "Swiss wastewater Surveillance Data Analyses"
description: "Analysis of wastewater samples collected at different Swiss wastewater treatment plants."
image: dashboards/covspectrum_logo.png
redirect_url: https://cov-spectrum.org/stories/wastewater-in-switzerland/
highlight: true
tags:
    - Surveillance
    - Wastewater
---

