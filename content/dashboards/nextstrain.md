---
title: "Real-time tracking of pathogen evolution"
description: "Nextstrain is an open-source project to harness the scientific and public health potential of pathogen genome data."
image: dashboards/nextstrain_logo.jpg
redirect_url: https://nextstrain.org/
tags:
    - Mpox
    - Ebola
    - Viruses
    - SARS-CoV-2
    - Influenza
---

