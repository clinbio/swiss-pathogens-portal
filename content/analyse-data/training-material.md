---
title: Bioinformatics resources
menu: 
  analyse_data:
    name: Training material
    identifier: training_material
    weight: 2
---

We list below material of interest for training and capacity building in FAIR pathogen data management and analysis.

- [The International cookbook for wastewater practitioners - Vol. 1 SARS-CoV-2, Aug 2024.](https://publications.jrc.ec.europa.eu/repository/handle/JRC138489)