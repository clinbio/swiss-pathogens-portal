---
title: Cite us
menu:
  navbar_top:
    name: Cite us
    weight: 5
---

## Research Resource Identifier of the Swiss Pathogens Portal

The Resource Identification Portal was created in support of the Resource Identification Initiative. It aims to promote the identification, discovery, and reuse of research resources. Research Resource Identifiers (RRIDs) are persistent and unique identifiers for referencing a research resource.

The RRID for the Swiss Pathogens Portal is **SCR_025457**.

By citing the portal using the RRID, you will facilitate further reuse of the portal, enable us to track that activity, and allow others to easily find the Summary Report for usage of the Swiss Pathogens Portal.