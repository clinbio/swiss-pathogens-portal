---
title: Contact and Suggestions
menu:
  navbar_top:
    name: Contact
    weight: 2
---

We welcome questions and suggestions regarding the Swiss Pathogens Portal. You may as well contact us if you need help with pathogen data management, sharing and analysis.

{{< contact_form >}}
