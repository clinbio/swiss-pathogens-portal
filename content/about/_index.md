---
title: About the Swiss Pathogens Portal
menu:
  navbar_top:
    name: About
    weight: 1
  footer_about:
    name:  Overview
    weight: 1
layout: about_navbar
---

## Vision

Rapid and open pathogen data sharing is essential to advance research and drive effective surveillance activities. Access to comprehensive data, including genomic sequences, epidemiological information, and clinical data, empowers researchers to track the spread and evolution of pathogens in real-time. This transparency is particularly critical for studying emerging viruses, food-borne pathogens, and antimicrobial-resistant bacteria, which pose significant and complex threats to public health.

A One Health approach, which integrates human, animal, and environmental health perspectives, is crucial in addressing these challenges. By facilitating the sharing of diverse data types across disciplines, this approach promotes a holistic understanding of pathogen dynamics and fosters coordinated research efforts to develop innovative solutions. Rapid and open data sharing not only enhances our ability to respond swiftly to health crises but also accelerates scientific discovery and strengthens global health security and preparedness.

## Aims

The Swiss Pathogens Portal was developed to support researchers in realizing this vision, by notably:
* Providing a one-stop resource for all Swiss pathogen data and projects.
* Fostering collaborations and synergies by creating a forum for discussion.
* Providing documentation and training material on FAIR research data management, especially for pathogen omics data.
* Showcasing Swiss resources that contribute to pathogen data sharing, access, analysis, and interpretation.