---
title: Programs and Organizations behind the Portal
menu:
  footer_about:
    name:  Programs and Organizations behind the Portal
    weight: 2
layout: about_navbar
---

{{< organization img="./sib_logo.png" name="SIB Swiss Institute of Bioinformatics - Centre for Pathogen Bioinformatics">}}
SIB is a leading organization in biological and biomedical data science. It maximizes the impact of, and investments in scientific projects by making data FAIR (Findable, Accessible, Interoperable and Reusable), provides long-term access to leading Swiss biodata resources, and is a high-quality partner enabling innovation in academia, hospitals and industry. It does this through its complementary pillars on coordination, open databases and software tools, and centre of excellence.

The Swiss Pathogens Portal is developed by the SIB Centre for Pathogen Bioinformatics, in collaboration with other SIB groups (e.g. Training).
{{< /organization >}}

{{< organization img="./bycovid_logo.png" name="BY-COVID">}}
SIB is one of the partners of the BY-COVID Consortium. The Swiss Pathogens Portal was developed and funded within BY-COVID.

The BeYond-COVID project aims to make COVID-19 data accessible to scientists in laboratories but also to anyone who can use it, such as medical staff in hospitals or government officials.

Going beyond SARS-CoV-2 data, the project will provide a framework for making data from other infectious diseases open and accessible to everyone.
{{< /organization >}}