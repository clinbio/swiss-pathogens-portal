---
title: Programs and Organizations behind the Portal
menu:
  footer_about:
    name:  Partners
    weight: 3
layout: about_navbar
---

{{< organization img="./pathogens_portal_logo.png" name="Pathogens Portal">}}
The Pathogens Portal, launched in July 2023, is an invaluable resource for researchers, clinicians, and policymakers who need access to the latest and most comprehensive datasets on pathogens. The portal is a collaborative effort between the European Molecular Biology Laboratory's European Bioinformatics Institute (EMBL-EBI) and partners. The Pathogens Portal provides a centralised platform for sharing and accessing data on key pathogens, including those that can cause diseases in humans and animals, and on vectors and hosts. By bringing together data from multiple sources, and linking these data where possible, the portal enables researchers to better understand the biology and life cycles of pathogens. These pathogens include both pathogens affecting humans and animals, and on vectors and hosts. It is funded through five European Union projects funded under the Horizon 2020 and Horizon Europe programmes: RECODID, VEO, EOSC-Life, CONVERGE and BY-COVID.

The national Pathogens Portals provide information, guidelines, tools and services to support researchers in creating and sharing research data on all pathogens and diseases. The purpose of the national portals is to showcase and highlight pathogen research data from each of the participating countries. Similar to the Pathogens Portal, the national portals rely on open and fair access of sequence and other omics data. The national portals are regularly updated with new tools, services and data.
{{< /organization >}}

{{< organization img="./swe_pathogens_portal_logo.png" name="The Swedish Pathogens Portal">}}
The Swedish Pathogens Portal provides information about available datasets, resources, tools, and services related to pandemic preparedness in Sweden.

The Portal is operated by the SciLifeLab Data Centre and partners, who developed an open reference implementation for national data portals using Hugo in combination with Bootstrap for styling, DataTables for tables, and Vega/Vega-Lite and Plotly for interactive graphics. All code used for the Swedish Portal is open source (held under an MIT licence) and is available on GitHub. The Swiss Pathogens Portal reused the code from the Swedish Portal.
{{< /organization >}}

{{< organization img="./elixir_logo.png" name="ELIXIR">}}
ELIXIR is an intergovernmental organisation that brings together life science resources from across Europe. These resources include databases, software tools, training materials, cloud storage and supercomputers. Under Web portals (https://elixir-europe.org/what-we-offer/portals) you will find useful portals to support you in making your data and software FAIR. 
{{< /organization >}}